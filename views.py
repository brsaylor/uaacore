# Copyright 2014 University of Alaska Anchorage Experimental Economics
# Laboratory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import get_script_prefix
from django.contrib.auth.views import password_change, password_change_done
from django.core.urlresolvers import reverse

from vcweb import core
from vcweb.core.models import Experiment, ParticipantExperimentRelationship, \
        is_experimenter, is_participant
from vcweb.core.decorators import experimenter_required, participant_required

import logging
logger = logging.getLogger('vcweb')

@login_required
def dashboard(request):
    """ Custom dashboard view which overrides the participant dashboard with a
    custom one that lists all active experiments and allows participants to join
    them. Simply calls the standard vcweb dashboard view if the user is an
    experimenter. """
    if is_experimenter(request.user):
        return core.views.dashboard(request)
    else:
        participant = request.user.participant
        current_experiments = (Experiment.objects
                .filter(is_visible=True)
                .exclude(status='COMPLETED'))
        joined_experiments = set([rel.experiment for rel in
            (ParticipantExperimentRelationship.objects
                .filter(participant=participant)
                .exclude(experiment__status='COMPLETED'))])
        return render(request, 'uaacore/participant/dashboard.html', {
            'joined_experiments': joined_experiments,
            'current_experiments': current_experiments,
            })

@participant_required
def join_experiment(request, pk=None):
    """ A participant posts to this view to join or rejoin an experiment.  If
    the experiment has an authentication code and the participant has not
    already joined, the submitted authentication code is checked. On success,
    the participant is redirected to the experiment's participant URL. """

    experiment = get_object_or_404(Experiment.objects, pk=pk)

    # If the participant has already joined this experiment, redirect to the
    # participant URL.
    try:
        per = ParticipantExperimentRelationship.objects.get(
                participant=request.user.participant,
                experiment=experiment)
        return redirect(experiment.participant_url)

    # Otherwise, check the authentication code, if required, add the participant
    # to the experiment, and redirect to the participant URL.
    except ParticipantExperimentRelationship.DoesNotExist:

        if (experiment.authentication_code is not None and
                request.POST.get('authentication_code', '').lower() !=
                experiment.authentication_code.lower()):
            messages.error(request,
                    "The authentication code you entered was incorrect.")
            return redirect('uaacore:dashboard')
        else:
            # Correct authentication code was entered
            # Add participant to experiment
            ParticipantExperimentRelationship.objects.create(
                    participant=request.user.participant,
                    experiment=experiment,
                    created_by=request.user)
            return redirect(experiment.participant_url)

@experimenter_required
def monitor(request, pk=None):
    """ Redirect the monitor URL to the (obsolete?) experiment.management_url,
    because that's what we use for the banking experiment.
    vcweb.core.views.monitor doesn't seem to provide a way to implement custom
    monitor interfaces. """
    experiment = get_object_or_404(Experiment.objects, pk=pk)
    return redirect(experiment.management_url)

def change_password(request):
    return password_change(request,
            post_change_redirect=reverse('uaacore:password_changed'))

def password_changed(request):
    return password_change_done(request)
